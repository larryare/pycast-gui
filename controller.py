from pyamaha import Device, System, Zone


class Controller:
    def __init__(self, device=None):
        self.device_ip = "192.168.8.104"
        self.zone = "main"
        self.dev = Device(self.device_ip)
        self.volume = self.get_volume()

    def vol_up(self):
        self.dev.request(Zone.set_volume(self.zone, "up", 2))
        self.get_volume()

    def vol_down(self):
        req = self.dev.request(Zone.set_volume(self.zone, "down", 2))
        self.get_volume()

    def set_volume(self, volume):
        req = self.dev.request(Zone.set_volume(self.zone, volume, None))
        self.get_volume()

    def get_volume(self):
        req = self.dev.request(Zone.get_status(self.zone))
        dic = req.json()
        return int(dic['volume'] / dic['max_volume'] * 100)

    def set_input(self):
        req = self.dev.request(Zone.set_input(self.zone, 'optical1', None))

    def get_zone_status(self):
        req = self.dev.request(Zone.get_status(self.zone))
        print(req.json())

    def set_power(self):
        # on, standby or toggle
        req = self.dev.request(Zone.set_power(self.zone, 'toggle'))

    def get_sound_program_list(self):
        req = self.dev.request(Zone.get_sound_program_list(self.zone))
        print(req.json())

# if __name__ == '__main__':
#     con = Controller()
#     con.set_volume(60)

