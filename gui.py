from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QSlider
import sys
from PyQt6.QtGui import QIcon
from PyQt6.QtCore import Qt, QSize
from controller import Controller

con = Controller()


class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('PyCast GUI')
        #self.icon = QIcon.addFile(self, 'icon.png', QSize(256, 256))
        self.setWindowIcon(QIcon('icon.png'))
        self.setFixedHeight(200)
        self.setFixedWidth(230)

        self.create_buttons()
        self.create_vol_label()

    vol_text = con.volume

    def create_vol_label(self):
        self.volume = QLabel(str(self.vol_text), self)
        self.volume.move(100, 20)

    def make_vol(self, value):
        volume = round(value * 121 / 100)
        con.set_volume(volume)
        self.volume.setText(str(round(volume / 121 * 100)))

    def create_buttons(self):
        btn_up = QPushButton('up', self)
        btn_up.clicked.connect(con.vol_up)

        btn_down = QPushButton('down', self)
        btn_down.clicked.connect(con.vol_down)
        btn_down.move(0, 50)

        btn_pwr = QPushButton('Power', self)
        btn_pwr.clicked.connect(con.set_power)
        btn_pwr.move(0, 100)

        btn_input = QPushButton('Optical', self)
        btn_input.clicked.connect(con.set_input)
        btn_input.move(0, 150)

        slider = QSlider(Qt.Orientation.Vertical, self)
        slider.setMaximum(100)
        slider.setMinimum(0)
        slider.setSingleStep(1)
        slider.setTickInterval(10)
        slider.setValue(con.volume)
        slider.setGeometry(10, 10, 50, 200)
        slider.move(140, 0)
        slider.valueChanged.connect(lambda: self.make_vol(slider.value()))

        


app = QApplication([])
window = Window()
window.show()
sys.exit(app.exec())